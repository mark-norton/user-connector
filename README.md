# user-connector

## Description

This repository contains an example of a Mulesoft Smart connector, as documented in:
https://docs.mulesoft.com/mule-sdk/1.1/xml-sdk

The example makes use of the JsonPlaceholder website, in particular:
https://jsonplaceholder.typicode.com/users

This returns user records in the form of:

``` json
[
  {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
          "street": "Kulas Light",
          "suite": "Apt. 556",
          "city": "Gwenborough",
          "zipcode": "92998-3874",
          "geo": {
                "lat": "-37.3159",
                "lng": "81.1496"
          }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
          "name": "Romaguera-Crona",
          "catchPhrase": "Multi-layered client-server neural-net",
          "bs": "harness real-time e-markets"
    }
  } ...
]
```

These records are converted into a local form like this:

``` json
{
    "userId": 1,
    "fullName": "Leanne Graham",
    "userName": "leanne",
    "email": "leanne@april.biz",
    "address": {
              "address1": "14 Main Street",
              "address2": "Apt. 556",
              "city": "Gwenborough",
              "postalCode": "92998-3874"
    },
    "phone": "1-770-736-8031 x56442",
}
```

